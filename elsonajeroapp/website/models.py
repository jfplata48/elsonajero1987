from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField
import os
from datetime import datetime

# Create your models here.
class Pai(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    def __str__(self):
        return self.Nombre

class Departamento(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    NombrePais= models.ForeignKey(Pai, blank=True, null=True)
    def __str__(self):
        return self.Nombre

class Categoria(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    orden = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)
    slug = models.SlugField(max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            #Only set the slug when the object is created.
            self.slug = slugify(self.nombre) #Or whatever you want the slug to use
        super(Categoria, self).save(*args, **kwargs)
        
    def __str__(self):
        return self.Nombre

class TipoContenido(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    icon = models.FileField("Icono", upload_to="icons/",default="")
    icon_hover = models.FileField("Icono Hover", upload_to="icons/",default="")
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)
    def __str__(self):              
        return self.Nombre


class TipoDestacado(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    orden = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)
    def __str__(self):
        return self.Nombre 

class EstadoNoticia(models.Model):
    Nombre = models.CharField(max_length = 80, blank = False, null=False)
    def __str__(self):
        return self.Nombre

def update_filename(instance, filename):
    path = "noticias/video/"
    filename = filename.split(".")
    format = slugify(filename[0])
    format = format + '.mp4'
    return os.path.join(path, format)

class Especial(models.Model):
    titulo = models.CharField(max_length=500)
    descripcion = models.CharField(max_length=1000, blank=True, null=True)
    imagen_portada = models.FileField("Imagen Portada", upload_to="noticias/imagenesportada/",default="")
    orden = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField('Fecha de publicación', auto_now_add=True, blank=True, null=True)
    fecha_inicio_publicacion = models.DateTimeField('Fecha de inicio de publicación',default=datetime.now)
    fecha_final_publicacion = models.DateTimeField('Fecha de finalización de publicación',default=datetime.now)
    slug = models.SlugField(max_length=200, blank=True, null=True)
    def save(self, *args, **kwargs):
        if not self.slug:
            #Only set the slug when the object is created.
            self.slug = slugify(self.titulo) #Or whatever you want the slug to use
        super(Especial, self).save(*args, **kwargs)
    def __str__(self):              
        return self.titulo
        
class NoticiaAct(models.Model):
    Titulo = models.CharField(max_length = 100, blank = False, null=False)
    Descripcion = models.CharField(max_length = 350, blank = False, null=False)
    imagen_portada = models.FileField("Imagen Portada", upload_to="noticias/imagenesportada/",default="")
    Fuente = models.CharField(max_length = 60, blank = False, null=False) 
    TipoContenido = models.ForeignKey(TipoContenido, blank = False, null=False)
    Audio = models.FileField("Audio", upload_to="noticias/audio/", blank=True, null=True)
    Video = models.FileField("Vídeo", upload_to=update_filename, blank=True, null=True)
    Texto =RichTextField(max_length = 10000, blank = False, null=False)
    html = models.CharField(max_length = 10000, blank = True, null=True) 
    Categorias = models.ManyToManyField(Categoria, blank = False, null=False)
    EstadoNoticia = models.ForeignKey(EstadoNoticia, blank = False, null=False)
    TipoDestacado = models.ManyToManyField(TipoDestacado, blank = False, null=False)
    CHOICES = ((1,'Principal'),(2,'Destacado 1'),(3,'Destacado 2'),(4,'Destacado 3'))
    OrdenDestacado = models.IntegerField(choices=CHOICES, default=None, blank=True, null=True)
    Pais = models.ForeignKey(Pai, blank = False, null=False)
    Departamento = models.ForeignKey(Departamento, blank = False, null=False)
    orden = models.IntegerField(default=0)
    viral = models.BooleanField(default=False)
    pidolapalabra = models.BooleanField(default=False)
    cronica = models.BooleanField(default=False)
    sesentasegundos = models.BooleanField(default=False)
    vimeoState = models.IntegerField(default=0)
    vimeoId = models.CharField(max_length=200,default=None, blank=True, null=True)
    fechaCreacion = models.DateTimeField('Fecha de publicación', auto_now_add=True, blank=True, null=True)
    fechaInicioPublicacion = models.DateTimeField('Fecha de inicio de publicación', blank=True, null=True)
    fechaFinalPublicacion = models.DateTimeField('Fecha de finalización de publicación', blank=True, null=True)
    especial = models.ForeignKey(Especial, blank=True, null=True)
    author = models.ForeignKey(User, null=True, blank=True)
    slug = models.SlugField(max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.Titulo)
        self.fechaCreacion= datetime.now()
        super(NoticiaAct,self).save(*args,**kwargs)
    def __str__(self):              
        return self.Titulo

class Archivo(models.Model):
    noticia = models.ForeignKey(NoticiaAct, blank=True, null=True)
    archivo = models.FileField(upload_to="noticias/archivos/")
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)

class Referencia(models.Model):
    noticia = models.ForeignKey(NoticiaAct, blank=True, null=True)
    fuente = models.CharField(max_length=500,default="")
    url = models.CharField(max_length=500,default="")
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)

class Tag(models.Model):
    noticia = models.ForeignKey(NoticiaAct, blank=True, null=True)
    tag = models.CharField(max_length=200,default="")
    slug = models.SlugField(max_length=200, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True, null=True)
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.tag)
        super(Tag, self).save(*args, **kwargs)

class Social_Icon(models.Model):
    nombre = models.CharField(max_length=200)
    icon = models.FileField("Icono", upload_to="icons/",default="")
    url = models.CharField(max_length=500)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True)
    def __str__(self):              
        return self.nombre

class Usuario(models.Model):
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    telefono = models.CharField(max_length=200)
    contrasena = models.CharField(max_length=1000)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True)
    def __str__(self):              
        return self.email

class UsuarioCategorias(models.Model):
    categoria = models.ForeignKey(Categoria, blank=True, null=True)
    usuario = models.ForeignKey(Usuario, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Fecha de creación', auto_now_add=True, blank=True)
    def __str__(self):              
        return self.usuario.email

class Stat(models.Model):    
    categoria = models.ForeignKey(Categoria, blank=True, null=True)
    noticia = models.ForeignKey(NoticiaAct, blank=True, null=True)
    url = models.CharField(max_length=500)
    ip = models.CharField(max_length=200)
    fecha = models.DateTimeField('Fecha', auto_now_add=True, blank=True)
    def __str__(self):              
        return str(self.ip) + ' - ' + str(self.url)























