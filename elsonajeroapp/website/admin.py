from django.contrib import admin
from django import forms
from django.contrib.auth.models import User, Group
from django.utils.safestring import mark_safe
from django.template import RequestContext 
from django.shortcuts import render_to_response
from django.conf.urls import include, url
from .models import Pai
from .models import Departamento
from .models import Categoria
from .models import TipoContenido
from .models import TipoDestacado
from .models import Tag
from .models import EstadoNoticia
from .models import Especial
from .models import NoticiaAct
from .models import Archivo
from .models import Referencia
from .models import Social_Icon
from .models import Usuario
from .models import Stat 

def __init__(self, user, *args, **kwargs):
    self.user = user
    super(RSVPForm, self).__init__(*args, **kwargs)

class NoticiaModelForm( forms.ModelForm ):      
    html = forms.CharField(widget=forms.Textarea(attrs={'cols': 100, 'rows': 20}),required=False) 
    class Meta:
         model = NoticiaAct         
         exclude = ('author', 'slug', 'fecha_vimeo', 'vimeo_state')

class NoticiaModelFormPeriodista( forms.ModelForm ):
    html = forms.CharField(widget=forms.Textarea(attrs={'cols': 100, 'rows': 20}),required=False)
    class Meta:
         model = NoticiaAct
         exclude = ('EstadoNoticia', 'author', 'slug', 'fecha_vimeo', 'vimeo_state')

class ArchivoInline(admin.StackedInline):
    model = Archivo
    extra = 3

class ReferenciaInline(admin.StackedInline):
    model = Referencia
    extra = 3

class TagInline(admin.StackedInline):
    model = Tag
    extra = 3


class NoticiaAdmin(admin.ModelAdmin):    
    search_fields = ['Titulo']
    change_list_template = 'admin/change_list_editor.html'
    inlines = [ArchivoInline, ReferenciaInline, TagInline]
       
    def button(self, obj):
        return mark_safe('<a href="/teleprompter/'+str(obj.id)+'"/" >Presentar</a>')

    def previsualizar(self, obj):
        return mark_safe('<a href="/noticia/'+str(obj.slug)+'"/" target="_blank">Previsualizar</a>')

    def destacado(self, obj):
        html = '<select class="ordendestacado" id="'+str(obj.id)+'">'
        html += '<option value="">Ninguno</option>'
        if obj.OrdenDestacado == 1:
            html += '<option value="1" selected="selected">Principal</option>'
        else:
            html += '<option value="1">Principal</option>'
        if obj.OrdenDestacado == 2:
            html += '<option value="2" selected="selected">Destacado 1</option>'
        else:
            html += '<option value="2">Destacado 1</option>'
        if obj.OrdenDestacado == 3:
            html += '<option value="3" selected="selected">Destacado 2</option>'
        else:
            html += '<option value="3">Destacado 2</option>'
        if obj.OrdenDestacado == 4:
            html += '<option value="4" selected="selected">Destacado 3</option>'
        else:
            html += '<option value="4">Destacado 3</option>'

        return mark_safe(html)

    def preview(self, obj):
        if obj.imagen_portada != '' and obj.imagen_portada != None:
            return mark_safe('<img class="preview" src="'+obj.imagen_portada.url+'"" width="300"/>')
        else:
            return mark_safe('<h3>Imagen de portada por definir</h3>')

    def get_queryset(self, request):
        qs = NoticiaAct.objects.all()
        if request.user.groups.all()[0].name != 'Periodista':
            return qs
        return qs.filter(author=request.user)

    def changelist_view(self, request, obj=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Editor' or request.user.groups.all()[0].name == 'Editor':
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion', 'button', 'destacado', 'previsualizar')            
        else:
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion', 'previsualizar')
        if request.user.groups.all()[0].name == 'Periodista - Editor':
            q = request.GET.copy()
            q['author__id'] = request.user.id
            request.GET = q
            request.META['QUERY_STRING'] = request.GET.urlencode()
            extra_context = {}
            extra_context['active'] = 2
            return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)
        extra_context = {}
        extra_context['active'] = 1
        return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)

    def get_form(self, request, obj=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Ciudadano':
            kwargs['form'] = NoticiaModelFormPeriodista            
        else:
            kwargs['form'] = NoticiaModelForm            
        return super(NoticiaAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self,  request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Ciudadano':
            if getattr(obj, 'EstadoNoticia', None) is None:
                estadoobj = EstadoNoticia.objects.get(id=2)
                obj.estado = estadoobj
        obj.save()

    def get_urls(self):
        urls = super(NoticiaAdmin, self).get_urls()
        my_urls = [
            url(r'^misnoticias/$', self.misnoticias),
            url(r'^porrevisar/$', self.porrevisar),
            url(r'^pidolapalabra/$', self.pidolapalabra),
            url(r'^programadas/$', self.programadas),
        ]
        return my_urls + urls

    def misnoticias(self, request, extra_context=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Editor' or request.user.groups.all()[0].name == 'Editor':
            self.list_display=('preview', 'Titulo', 'EstadoNoticia', 'fechaCreacion', 'button')            
        else:
            self.list_display=('preview', 'Titulo', 'EstadoNoticia', 'fechaCreacion')
        q = request.GET.copy()
        q['author__id'] = request.user.id
        request.GET = q
        request.META['QUERY_STRING'] = request.GET.urlencode()
        extra_context = {}
        extra_context['active'] = 2
        return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)

    def porrevisar(self, request, extra_context=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Editor' or request.user.groups.all()[0].name == 'Editor':
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion', 'button')            
        else:
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion')
        q = request.GET.copy()
        q['estado__id'] = 2
        if request.user.groups.all()[0].name == 'Periodista - Editor':
            q['author__id'] = request.user.id 
        request.GET = q
        request.META['QUERY_STRING'] = request.GET.urlencode()
        extra_context = {}
        extra_context['active'] = 3
        return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)

    def pidolapalabra(self, request, extra_context=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Editor' or request.user.groups.all()[0].name == 'Editor':
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion', 'button')            
        else:
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion')
        q = request.GET.copy()
        q['pidolapalabra'] = True
        request.GET = q
        request.META['QUERY_STRING'] = request.GET.urlencode()
        extra_context = {}
        extra_context['active'] = 4
        return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)

    def programadas(self, request, extra_context=None, **kwargs):
        if request.user.groups.all()[0].name == 'Periodista' or request.user.groups.all()[0].name == 'Periodista - Editor' or request.user.groups.all()[0].name == 'Editor':
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion', 'button')            
        else:
            self.list_display=('preview', 'Titulo', 'EstadoNoticia','fechaCreacion')
        q = request.GET.copy()
        q['programada'] = True
        if request.user.groups.all()[0].name == 'Periodista - Editor':
            q['author__id'] = request.user.id
        request.GET = q
        request.META['QUERY_STRING'] = request.GET.urlencode()
        extra_context = {}
        extra_context['active'] = 5
        return super(NoticiaAdmin, self).changelist_view(request, extra_context=extra_context)

    def get_urls(self):
        urls = super(NoticiaAdmin, self).get_urls()
        my_urls = [
            url(r'^misnoticias/$', self.misnoticias),
            url(r'^porrevisar/$', self.porrevisar),
            url(r'^pidolapalabra/$', self.pidolapalabra),
            url(r'^programadas/$', self.programadas),
        ]
        return my_urls + urls

# Register your models here.
admin.site.register(Pai)
admin.site.register(Departamento)
admin.site.register(Categoria)
admin.site.register(TipoContenido)
admin.site.register(TipoDestacado)
admin.site.register(Tag)
admin.site.register(EstadoNoticia)
admin.site.register(Especial)
admin.site.register(NoticiaAct, NoticiaAdmin)
admin.site.register(Referencia)
admin.site.register(Social_Icon)