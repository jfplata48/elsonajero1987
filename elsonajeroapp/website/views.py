from django.shortcuts import render
from .models import Categoria
from .models import Social_Icon
from .models import NoticiaAct
from .models import Archivo
from .forms import ArchivoForm
from .models import Referencia
from .models import Especial
from .models import Tag
from .models import Stat
from .models import *
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.views.generic.list import ListView
from django.http import JsonResponse
import facebook
import tweepy
import os
import datetime
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils import timezone
import vimeo
import datetime
import calendar
from datetime import date
from django.shortcuts import redirect
from django.db.models import Q
from django.db import connections
from datetime import timedelta



def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]
# Create your views here.
def index(request):
	time_threshold = timezone.now()
	currentTime = datetime.datetime.now()
	yesterday = date.today() - timedelta(1)
	yesterday = yesterday.strftime('%Y-%m-%d')
	cursor = connections['default'].cursor()
	categorias = Categoria.objects.all()
	noticia = NoticiaAct.objects.all()
	noticia1 = NoticiaAct.objects.all().order_by("-id")[1:3]
	contnoticia = NoticiaAct.objects.all().order_by('fechaFinalPublicacion').count()
	noticiass = NoticiaAct.objects.all()[contnoticia-1]
	context={
		'categorias':categorias,
		'noticia':noticia,
		'noticia1':noticia1,
		'noticiass':noticiass,
	}

	return render(request,'website/inicio.html',context)

