// Website Functionality
var website = {
    init: function( settings ) {
        
        website.config = {
            sliders: [$("#layerslider" ),$("#layerslider2"),$("#layerslider3")],
            isscrolled: false, 
            header: $("header"),
            timer: null,
            videoelement: null,
            ismobile: false,
        };
 
        // Allow overriding the default config
        $.extend( website.config, settings ); 
        website.setup();
        website.initevents();
        
        //reloj
        var monthNames = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]; 
        var dayNames= ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"]
        var newDate = new Date();
        newDate.setDate(newDate.getDate());
        $('#datedesc').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

        setInterval( function() {
            // Create a newDate() object and extract the seconds of the current time on the visitor's
            var seconds = new Date().getSeconds();
            // Add a leading zero to seconds value
            $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
            },1000);
    
        setInterval( function() {
            // Create a newDate() object and extract the minutes of the current time on the visitor's
            var minutes = new Date().getMinutes();
            // Add a leading zero to the minutes value
            $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
            },1000);
    
        setInterval( function() {
            // Create a newDate() object and extract the hours of the current time on the visitor's
            var hours = new Date().getHours();
            // Add a leading zero to the hours value
            $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
            }, 1000); 


        //carrusel

        
        var owl = $('.carousel-lastnews');
        owl.owlCarousel({autoplay: true, autoplayHoverPause: true, items: 2, loop: true, autoplayTimeout:3000, stagePadding:14, margin:14, responsive:{
        0:{
            items:2,
            nav:false
        },
        680:{
            items:2,
            nav:false
        },
        1000:{
            items:2,
            nav:false,
        }
        }});

        $('.next').click(function() {
            owl.trigger('next.owl.carousel');
        });

        $('.prev').click(function() {
            owl.trigger('prev.owl.carousel');
        });

        var owl2 = $('.carousel');
        owl2.owlCarousel({autoplay: false, autoplayHoverPause: false, items: 5, autoplayTimeout:3000, stagePadding:14, margin:14, responsive:{
        0:{
            items:2,
            nav:false
        },
        680:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:false,
        }
        }}); 

        $('.next2').click(function() {
            owl2.trigger('next.owl.carousel');
        });

        $('.prev2').click(function() {
            owl2.trigger('prev.owl.carousel');
        });

        //carrusel responsive

        $('.owl-carousel-resp').owlCarousel({autoplay: true, items: 1, loop: true, autoplayTimeout:3000, stagePadding:0, margin:0});
        
        //lazyload

        $("img.lazy").lazyload();

        /*var $someImages = $('img.lazy');
        objectFitImages($someImages);*/

        if ( ! Modernizr.objectfit ) {
          $('.newimage').each(function () {
            var $container = $(this),
                imgUrl = $container.find('.newbg').attr('data-original');
            if (imgUrl) {
              $container
                .css('backgroundImage', 'url(' + imgUrl + ')')
                .addClass('compat-object-fit');
            } 
            else{
                imgUrl = $container.find('.newbg').prop('src');
                if (imgUrl) {
                $container
                .css('backgroundImage', 'url(' + imgUrl + ')')
                .addClass('compat-object-fit');
                }
            } 
          });
        }        

    },
 
    setup: function() {
        var sliders = website.config.sliders;
        /*for (index = 0; index < sliders.length; ++index) {
            sliders[index].layerSlider({
            pauseOnHover: false,
            autoPlayVideos: false,
            });
        }*/

        /*plyr.setup(".video");
        plyr.setup(".audio");*/

        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            website.config.ismobile = true;
        }            
    }, 

    showmore: function() {
        $("#videosugestions").css("bottom", "60px");
    },

    showless: function() {
        $("#videosugestions").css("bottom", "-140px");
    },

    showplayer: function(element, icon) {
        
        website.config.videoelement = [element.offset().top,element.offset().left,element.width(), element.height()];
        $("#overlayvideo").css("display", "block");
        $("#overlayvideo").css("top", element.offset().top);
        $("#overlayvideo").css("left", element.offset().left);
        $("#overlayvideo").css("width", element.width());
        $("#overlayvideo").css("height", element.height());
        if (icon.attr("type") == "3")  {
           window.open(icon.attr("alt"), "_self");
        }
        else {
           $("#overlayvideo").html('<img id="close" src="/static/website/images/close.png"><video class="videooverlay" id="videooverlay" poster="'+icon.attr("poster")+'" controls autoplay><source src="'+icon.attr("name")+'" type="video/mp4"></video>');
        }
        $('.videooverlay').css("width", element.width());
        $('.videooverlay').css("height", element.height()); 
        $("#close").unbind();
        $("#close").click(function (e) { 
           $(this).parent().css("display", "none");
        });
        var player = plyr.setup('.videooverlay');  

    },

    sendnew: function() {
    
    var data = new FormData($('#newfiles').get(0));

    if($("#field1").val() != "" && $("#field2").val() != "" && $("#field3").val() != "" && $("#field4").val() != "" && $("#field5").val() != "" && $("#field6").val() != "" && $("#field7").val() != ""  && $("#field8").val() != "")  {

    $.ajax({
        url: '/sendnew/',
        type: 'POST',
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            
            $("#alertmsj").html("Se ha enviado tu noticia a revisión. pronto te comunicaremos cuando se publique.");
            $("#modal-6").addClass("md-show");
            setTimeout(hide, 5000);
            function hide() {
                $("#modal-6").removeClass("md-show");
            }
            
        }
    });

    }
    else{

        setTimeout(show, 500);
        function show() {
                $("#modal-2").addClass("md-show");
                $("#errormsj").css("display","block");
            }

    }

    },

    savepc: function() {

    $.post("/savepc/", {cat1: $("#categoria1").is(":checked"),cat2: $("#categoria2").is(":checked"),cat3: $("#categoria3").is(":checked"),cat4: $("#categoria4").is(":checked"),cat5: $("#categoria5").is(":checked"),cat6: $("#categoria6").is(":checked"),cat7: $("#categoria7").is(":checked"),cat8: $("#categoria8").is(":checked"),cat9: $("#categoria9").is(":checked")}, function(data){
        
            $("#alertmsj").html("Se han guardado tus preferencias para este navegador.");
            $("#modal-6").addClass("md-show");
            setTimeout(hide, 5000);
            function hide() {
                $("#modal-6").removeClass("md-show");
                location.reload();
            }
                
    });

    },

    initevents: function() {

    if (website.config.ismobile == false) {

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();    
        if (scroll == 0) {
            website.config.isscrolled = false;
            //website.config.header.removeClass("scrolled");
            var element = website.config.videoelement;
            if (element != null) {
            $("#overlayvideo").css("top", element[0]);
            $("#overlayvideo").css("left", element[1]);
            $("#overlayvideo").css("width", element[2]);
            $("#overlayvideo").css("height", element[3]);
            $('.videooverlay').css("width", element[2]);
            $('.videooverlay').css("height", element[3]);
            }
        }
        else{
            if(website.config.isscrolled == false)
            {
                website.config.isscrolled = true;  
                //website.config.header.addClass("scrolled");                
                $("#overlayvideo").css("left", "1000px");
                $("#overlayvideo").css("width", "320px");
                $("#overlayvideo").css("height", "180px");
                $('.videooverlay').css("width", "320px");
                $('.videooverlay').css("height", "180px"); 
            }
        }
    });
    
    }
    
    $("#buttonsend").click(function (e) {
       e.preventDefault;
       website.sendnew();
    }); 

    $("#guardarpc").click(function (e) {
       e.preventDefault;
       website.savepc();
    }); 

    $(".more").click(function (e) {
       $(".showmore").css("display","block");
    });

    $(".bottom").mouseleave(function () {
       $(".showmore").css("display","none");
    });  

    $(".text").mouseenter(function () {
       $(this).parent().parent().css("top","152px");
       $(this).parent().parent().css("z-index","0");
    });

    $(".special").mouseleave(function () {
       $(this).css("top","0px");
       $(this).css("z-index",$(this).attr("id"));
    });

    $(".ordendestacado").change(function () {
        
       var response = confirm("Desea cambiar el destacado de la noticia ?");

        if (response == true){
            alert($(this).att("id"));
        }

    }); 

    //search events

    $(".search").mouseleave(function () {
       $("#searchinput").css("display","none");
    });  

    $("#searchicon").mouseenter(function () {
       $("#searchinput").css("display","block");
    });

    //play radio

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', 'http://142.4.217.133:8222/cristalinaplus');
    
    isplaying = false;

    $("#playradio").click(function () {
        
       if (isplaying==false) {

           $("#playradio").attr("src","/static/website/images/pause_radio.png");
           isplaying = true;
           audioElement.play();

       } else{

           $("#playradio").attr("src","/static/website/images/play_radio.png");
           isplaying = false;
           audioElement.pause();

       };

    }); 

    //share events

    $(".facebookshare").click(function (e) {
       
        var link = $(this).attr("name");
        FB.ui({
        method: 'share',
        display: 'popup',
        href: link,
        }, function(response){});

    });


    $(".menu").click(function (e) {
       
        $("#modal-7").addClass("md-show");

    });

    $(".participe").click(function (e) {
       
        $("#modal-2").addClass("md-show");

    });

    $(".personalizar").click(function (e) {
       
        $("#modal-1").addClass("md-show");

    });

    $(".md-close").click(function (e) {
       
        $(".md-modal").removeClass("md-show");

    });

    $(".md-overlay").click(function (e) {
       
        $(".md-modal").removeClass("md-show");

    });

    $(".especialopen").click(function (e) {
       
        var url = $(this).attr("id");
        window.location = url;

    });

    //save stats 

    if ( $( "#categoria" ).length ) {

    var categoria = $("#categoria").val();
    var noticia = $("#noticia").val();
    var url = window.location.href;
    
    $.post("/savestat/", {categoria: categoria, noticia: noticia, url: url}, function(data) {            
        
        console.log(data.status);
        
    }); 

    }

    if ($('#categorynews').length > 0) {

    var uploadmore = true;
    var pagestart = 21;
    var categoryid = $("#categoryid").val();
    if (categoryid == 8) {
        pagestart = 31;
    }
    var token = $("input[name='csrfmiddlewaretoken'").val();

    $(window).scroll(function() {

       var hT = $('#categorynews').offset().top,
       hH = $('#categorynews').outerHeight(),
       wH = $(window).height(),
       wS = $(this).scrollTop();

       if (wS > (hT+hH-wH) & uploadmore == true) {
        
        uploadmore = false;
        console.log('load more news...');
        console.log(categoryid);
        console.log(uploadmore);
        console.log(pagestart);

        $.post("/loadmore/", {pagestart: pagestart, categoryid: categoryid, csrfmiddlewaretoken: token}, function(data){
            
            for (var i = 0; i < data.noticias.length; i++) {            
            
            $("#categorynews").append('<a href="/noticia/'+data.noticias[i].slug+'"> <article> <div class="content"> <div class="newimage"> <a href="/noticia/'+data.noticias[i].slug+'"> <img class="lazy newbg" src="'+data.noticias[i].imagen+'" alt="{{noticia.titulo}}"> <a href="/noticia/'+data.noticias[i].slug+'?autoplay=true"> <img class="icon playvideo" poster="/media/'+data.noticias[i].imagen+'" src="'+data.noticias[i].icono+'" alt=""> <img class="icon playvideo purple" title="{{noticia.titulo}}" poster="/media/{{noticia.imagen_portada}}" name="/media/{{noticia.video}}" src="'+data.noticias[i].iconohover+'" alt=""> </a> </a> </div><div class="newdescription"> <div class="top"> <a href="/noticia/'+data.noticias[i].slug+'"> <h1>'+data.noticias[i].titulo+'</h1> <p>'+data.noticias[i].descripcion+'</p></a> </div><div class="bottom"> <div class="left"> <p>'+data.noticias[i].categoria+'</p></div><div class="right"> <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.elsonajero.com/noticia/'+data.noticias[i].slug+'" target="_blank"> <img src="/static/website/images/facebook-new2.png"> <img class="purple" src="/static/website/images/facebook-blue-2.png"> </a> <a href="https://twitter.com/home?status=Mira%20esta%20noticia%3A%20http://www.elsonajero.com/noticia/'+data.noticias[i].slug+'" target="_blank"> <img src="/static/website/images/twitter-new2.png"> <img class="purple" src="/static/website/images/twitter-blue-2.png"> </a> <a href="whatsapp://send?text=Mira%20esta%20noticia%3A%20http://www.elsonajero.com/noticia/'+data.noticias[i].slug+'"> <img src="/static/website/images/whatsapp-new2.png"> <img class="purple" src="/static/website/images/whatsapp-blue-2.png"> </a> </div></div></div></div></article> </a>');
            
            };

            hT = $('#categorynews').offset().top;
            hH = $('#categorynews').outerHeight();
            wH = $(window).height();
            wS = $(this).scrollTop();
            pagestart += 20;

            setTimeout(function() {
                uploadmore = true;            
            }, 1000);
            
        
        });     
            

        }
    });

    }   

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    if (iOS == true) {

    $('a').on('touchmove touchend', function(e) {

    // if touchmove>touchend, set the data() for this element to true. then leave touchmove & let touchend fail(data=true) redirection
    if (e.type == 'touchmove') {
        $.data(this, "touchmove_cancel_redirection", true );
        return;
    }

    // if it's a simple touchend, data() for this element doesn't exist.
    if (e.type == 'touchend' && !$.data(this, "touchmove_cancel_redirection")) {
        var el = $(this);
        var link = el.attr('href');
        window.location = link;
    }

    // if touchmove>touchend, to be redirected on a future simple touchend for this element
    $.data(this, "touchmove_cancel_redirection", false );
    
    });

    }                    
                    
    },
};
 
$( document ).ready( function () {
   
$.when(
    $.getScript( "/static/website/js/jquery.lazyload.min.js" ),
    $.getScript( "/static/website/owlcarousel/owl.carousel.js?v=2" ),
    $.getScript( "/static/website/js/ofi.min.js" ),
    $.Deferred(function( deferred ){
        $( deferred.resolve );
    })
).done(function(){

    website.init();

});

});