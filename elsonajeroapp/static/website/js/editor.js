// Website Functionality
 
$( document ).ready(function(){

    $(".ordendestacado").change(function () {
        
       var response = confirm("Desea cambiar el destacado de la noticia ?");

        if (response == true){
        
        var idnoticia = $(this).attr("id");
        var orden_destacado = $(this).val();

        $.post("/changedestacado/", {idnoticia:idnoticia, orden_destacado: orden_destacado}, function(data){
        
            alert("Se ha modificado el destacado correctamente");
            //location.reload();
        
        });

        }

    }); 
    
});
